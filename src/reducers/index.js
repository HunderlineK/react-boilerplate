import { combineReducers } from "redux";

import { LOGIN, LOGOUT } from "../actions";

function authenticated(state, action) {
  switch (action.type) {
    case LOGIN:
      return Boolean(action.token);
    case LOGOUT:
    default:
      return false;
  }
}

const app = combineReducers({
  authenticated
});

export default app;
