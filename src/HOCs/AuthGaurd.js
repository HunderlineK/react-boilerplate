import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class AuthGaurd extends React.Component {
  static propTypes = {
    authenticated: PropTypes.bool,
    render: PropTypes.func,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    }).isRequired,
    shouldBe: PropTypes.bool
  };

  static defaultProps = {
    authenticated: false,
    render: () => <React.Fragment />,
    shouldBe: true
  };

  componentDidMount() {
    this.gaurd();
  }

  componentDidUpdate() {
    this.gaurd();
  }

  gaurd() {
    if (this.props.shouldBe !== this.props.authenticated) {
      this.props.history.push("/");
    }
  }

  render() {
    return this.props.render(this.props);
  }
}

const mapStateToProps = ({ authenticated }) => ({
  authenticated
});

export default connect(mapStateToProps)(AuthGaurd);
