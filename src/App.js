import React from "react";
import { hot } from "react-hot-loader";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";

import store from "./store";
import "./styles.css";

import Navbar from "./components/Navbar";
import Main from "./components/Main";
import ErrorBoundary from "./components/ErrorBoundary";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <React.Fragment>
          <ErrorBoundary>
            <Navbar />
          </ErrorBoundary>
          <ErrorBoundary>
            <Main />
          </ErrorBoundary>
        </React.Fragment>
      </Router>
    </Provider>
  );
}

export default hot(module)(App);
