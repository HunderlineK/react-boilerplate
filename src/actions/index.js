/*
 * action types
 */

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

/*
 * action creators
 */

export function login(token) {
  return { type: LOGIN, token };
}

export function logout() {
  return { type: LOGOUT, payload: null };
}
