import React from "react";

import withAuth from "./utils/withAuth";

class Area67 extends React.Component {
  state = {
    loading: true,
    json: null,
    error: null,
    statusText: null
  };

  constructor(props) {
    super(props);
    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    this.setState({
      loading: true,
      json: null,
      error: null,
      statusText: null
    });
    const url = "http://eu.httpbin.org/basic-auth/user/passwd";
    const username = "user";
    const password = "passwd";
    const init = {
      headers: {
        Authorization: `Basic ${btoa(`${username}:${password}`)}`
      }
    };
    fetch(url, init)
      .then(response => {
        this.setState({ loading: false, statusText: response.statusText });
        if (response.ok) {
          return response.json();
        }
      })
      .then(json => {
        if (json) {
          this.setState({ json, error: null });
        }
      })
      .catch(error => {
        this.setState({ error, json: null });
      });
  }

  render() {
    return (
      <React.Fragment>
        {!this.state.loading && (
          <button onClick={() => this.fetchData()}>Load data</button>
        )}
        {this.state.loading && <h1>Loading</h1>}
        {this.state.statusText && (
          <h1>{"status: " + this.state.statusText.toLowerCase()}</h1>
        )}
        {this.state.json && <h1>{JSON.stringify(this.state.json)}</h1>}
        {this.state.error && (
          <h1 style={{ color: "red" }}>{JSON.stringify(this.state.error)}</h1>
        )}
      </React.Fragment>
    );
  }
}

export default withAuth(Area67);
