import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { login } from "../actions";
import withAuth from "./utils/withAuth";

class Login extends Component {
  static propTypes = {
    authenticated: PropTypes.bool,
    handleSubmit: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired
  };

  static defaultProps = {
    authenticated: false
  };

  state = {
    email: null,
    password: null
  };

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit(this.state);
  }

  handleChange({ target: { name, value } }) {
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          required
          onChange={this.handleChange}
          name="email"
          type="email"
        />
        <input
          required
          onChange={this.handleChange}
          name="password"
          type="password"
        />
        <button type="submit">submit</button>
        <button>cancel</button>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    handleSubmit: ({ email, password }) => {
      dispatch(login({ email, password }));
    }
  };
};

export default withAuth(
  connect(
    null,
    mapDispatchToProps
  )(Login),
  false
);
