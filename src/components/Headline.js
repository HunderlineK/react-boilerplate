import React from 'react';
import PropTypes from 'prop-types';

function Headline({ children }) {
    return <h1>{children}</h1>;
}

Headline.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
      ])
}

Headline.defaultProps = {
    children: null,
}

export default Headline;
