import React, { Component } from "react";
import PropTypes from "prop-types";

class ErrorBoundary extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]).isRequired
  };

  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
    /* eslint-disable no-console */
    console.error(error);
    console.error(info);
    /* eslint-enable no-console */
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>An unexpected error has happened</h1>;
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
