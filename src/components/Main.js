import React from "react";
import { Route } from "react-router-dom";

import Login from "./Login";
import Area67 from './Area67';

function Main() {
  return (
    <React.Fragment>
      <Route path="/login" component={Login} />
      <Route path="/area-67" component={Area67} />
    </React.Fragment>
  );
}

export default Main;
