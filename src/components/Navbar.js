import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { logout } from "../actions";

function Navbar({ authenticated, logout }) {
  return (
    <nav>
      <Link to="/">Home</Link>
      {authenticated && <Link to="/area-67">Area 67</Link>}
      {authenticated ? (
        <Link to="home" onClick={logout}>
          Logout
        </Link>
      ) : (
        <Link to="/login">Login</Link>
      )}
    </nav>
  );
}

Navbar.propTypes = {
  authenticated: PropTypes.bool,
  logout: PropTypes.func.isRequired
};

Navbar.defaultProps = {
  authenticated: false
};

const mapStateToProps = ({ authenticated }) => {
  return {
    authenticated
  };
};

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar);
