import React from "react";

import AuthGaurd from "../../HOCs/AuthGaurd";

function withAuth(WrappedComponent, shouldBe) {
  function GaurdedComponent(props) {
    return (
      <AuthGaurd
        {...props}
        shouldBe={shouldBe}
        render={(_props) => <WrappedComponent {..._props} />}
      />
    );
  }
  return GaurdedComponent;
}

export default withAuth;
